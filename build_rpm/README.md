1. dnf install mock
2. cp default.cfg /etc/mock/
3. gpasswd -a your_user mock
4. edit /etc/mock/default.cfg and put TOKEN (https://abf.rosalinux.ru/platforms/rosa2019.05/tokens) there

python3 builder.py --file srcrpm.list

srcrpm.list content example:

/home/omv/2019.05/dos2unix-7.4.1-1.src.rpm
/home/omv/2019.05/libbluray-1.1.1-4.src.rpm


to build single package

python3 builder.py --srcrpm libfoo.src.rpm
