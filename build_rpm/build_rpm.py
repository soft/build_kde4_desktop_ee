import subprocess
import os
import sys
import argparse

mock_binary = '/usr/bin/mock'
mock_config = '/etc/mock/'
get_home = os.environ.get('HOME')
output_dir = get_home + '/output'

def print_log(message, log):
    print(message)
    try:
        logFile = open(log, 'a')
        logFile.write(message + '\n')
        logFile.close()
    except:
        print("Can't write to log file: " + log)


def run_local_builder(srcrpm):
    try:
        print('run local build with mock')
        subprocess.check_call([mock_binary, '-v', '--update', '--configdir', mock_config, '--rebuild', srcrpm, '--no-cleanup-after', '--no-clean', '--resultdir=' + output_dir])
    except subprocess.CalledProcessError as e:
        print_log('local build [{}] failed'.format(srcrpm), 'failed.log')
        print(e)
        sys.exit(1)
    print_log('{} build complete'.format(srcrpm), 'update.log')



if __name__ == '__main__':
   parser = argparse.ArgumentParser()
   parser.add_argument('--srcrpm', nargs='+', help='srcrpm to upgrade')
   parser.add_argument('--file', help='file with srcrpms list')
   args = parser.parse_args()
   if args.file is not None:
      with open(args.file) as file:
           for line in file:
               srcrpm = line.strip()
               run_local_builder(srcrpm)

   if args.srcrpm is not None:
      srcrpms = [i for i in args.srcrpm if i is not None]
      for srcrpm in srcrpms:
          run_local_builder(srcrpm)

